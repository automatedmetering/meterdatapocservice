public class BaseDTO
{
    public Guid Id { get; set; }
    public DateTimeOffset Created { get; set; }
}