public class PowerStatusDataDTO : BaseDTO
{
    public PowerStatus Status { get; set; }
    public string MeterNumber { get; set; }
    public string StatusName => Status.ToString();
    public bool IsLightUp { get; set; }
}