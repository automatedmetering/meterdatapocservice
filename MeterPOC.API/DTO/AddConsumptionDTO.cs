using System.ComponentModel.DataAnnotations;

public class AddConsumptionDTO
{
    [Range(100, 1000000)]
    public int MeterNumber { get; set; }

    [Required(ErrorMessage ="Reading in KwH is required")]
    [MaxLength(6, ErrorMessage ="Reading in Kwh should not be more than 6 digits")]
    public string ReadingKwH { get; set; }
}