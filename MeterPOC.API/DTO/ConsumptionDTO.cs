public class  ConsumptionDTO : BaseDTO
{
    public int MeterNumber { get; set; }
    public double ReadingKwH { get; set; }
}