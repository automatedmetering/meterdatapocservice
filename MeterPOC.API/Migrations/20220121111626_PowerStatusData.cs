﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeterPOC.API.Migrations
{
    public partial class PowerStatusData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PowerStatusDatas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    MeterNumber = table.Column<int>(type: "integer", nullable: false),
                    IsLightUp = table.Column<bool>(type: "boolean", nullable: false),
                    Created = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Modified = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    Deleted = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    DeletedBy = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PowerStatusDatas", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PowerStatusDatas_MeterNumber",
                table: "PowerStatusDatas",
                column: "MeterNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PowerStatusDatas");
        }
    }
}
