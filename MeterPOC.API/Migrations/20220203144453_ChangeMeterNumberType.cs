﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeterPOC.API.Migrations
{
    public partial class ChangeMeterNumberType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MeterNumber",
                table: "PowerStatusDatas",
                type: "text",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "MeterNumber",
                table: "PowerStatusDatas",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }
    }
}
