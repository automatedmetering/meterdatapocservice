using Microsoft.EntityFrameworkCore;

public class PowerStatusService : BaseService, IPowerStatusService
{
    public PowerStatusService(IUnitOfWork unitOfWork, IResponseService responseService)
        : base(unitOfWork, responseService) { }

    public async Task<ServiceResponse<string>> CreateAsync(CreatePowerStatusDTO model)
    {
        if(string.IsNullOrEmpty(model.MeterNumber))
            return _responseService.ErrorResponse<string>("Invalid meter number");
        if(model.MeterNumber.Length < 3)
            return _responseService.ErrorResponse<string>("Meter number should not be less than 3 characters");

        if(model.Status != PowerStatus.On && model.Status != PowerStatus.Off)
            return _responseService.ErrorResponse<string>("Invalid PowerStatus");
            
        var powerStatusData = new PowerStatusData
        {
            MeterNumber = model.MeterNumber,
            Status = model.Status,
            IsLightUp = model.IsLightUp,
            CreatedBy = "system"
        };
        await _unitOfWork.Context.AddAsync(powerStatusData).ConfigureAwait(false);
        await _unitOfWork.CommitAsync().ConfigureAwait(false);
        return _responseService.SuccessResponse("success");
    } 
    
    public async Task<ServiceResponse<PagedList<PowerStatusDataDTO>>> GetAsync(int page, int size)
    {
        var powerStatusData = _unitOfWork.Context.PowerStatusDatas.AsNoTracking()
            .OrderByDescending(x => x.Created).Select(x => new PowerStatusDataDTO
            {
                MeterNumber = x.MeterNumber,
                Status = x.Status,
                IsLightUp = x.IsLightUp,
                Id = x.Id,
                Created = x.Created,
            });
        
        return await _responseService.PagedResponseAsync(powerStatusData, page, size, "Power Status Data");
    }
}