using Microsoft.EntityFrameworkCore;

public class ConsumptionService : BaseService, IConsumptionService
{
    public ConsumptionService(IUnitOfWork unitOfWork, IResponseService responseService) : base(unitOfWork, responseService)
    {
    }

    public async Task<ServiceResponse<string>> AddAsync(AddConsumptionDTO model)
    {

        if(!double.TryParse(model.ReadingKwH, out var reading)) 
            return _responseService.ErrorResponse<string>("Invalid reading value");

        var consumption = new Consumption
        {
            MeterNumber = model.MeterNumber,
            ReadingKwH = reading
        };

        await _unitOfWork.Context.Consumptions.AddAsync(consumption).ConfigureAwait(false);
        await _unitOfWork.CommitAsync().ConfigureAwait(false);

        return _responseService.SuccessResponse("Consumption saved succeddfully..");
    }

    public async Task<ServiceResponse<PagedList<ConsumptionDTO>>> GetAsync(int page = 1, int size = 10)
    {
        var consumptions = _unitOfWork.Context.Consumptions.AsNoTracking()
            .OrderByDescending(x => x.Created)
            .Select(x => new ConsumptionDTO
            {
                Id = x.Id,
                Created = x.Created,
                MeterNumber = x.MeterNumber,
                ReadingKwH = x.ReadingKwH
            });
        return await _responseService.PagedResponseAsync(consumptions, page, size, "Consumptions");
    }
}