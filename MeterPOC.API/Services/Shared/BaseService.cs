public abstract class BaseService
{
    protected readonly IUnitOfWork _unitOfWork;
    protected readonly IResponseService _responseService;

    public BaseService(IUnitOfWork unitOfWork, IResponseService responseService)
    {
        _unitOfWork = unitOfWork;
        _responseService = responseService;
    }
}