using Microsoft.EntityFrameworkCore;

public interface IResponseService
{
    ServiceResponse<T> ErrorResponse<T>(string message) where T : class;
    ServiceResponse<T> SuccessResponse<T>(T data, string message = null);
    ServiceResponse<T> ExceptionResponse<T>(Exception exception) where T : class;
    Task<ServiceResponse<PagedList<T>>> PagedResponseAsync<T>(IQueryable<T> source,
        int page, int pageSize, string entityName) where T : class;
}

public class ResponseService : IResponseService
{
    public ServiceResponse<T> ErrorResponse<T>(string message) where T : class => new ServiceResponse<T> { Message = message ?? "An error occured" };

    public ServiceResponse<T> ExceptionResponse<T>(Exception exception) where T : class =>
        new ServiceResponse<T> { Message = exception.Message };

    public ServiceResponse<T> SuccessResponse<T>(T data, string message = null)
        => new ServiceResponse<T>
        {
            Message = message ?? "Operation successful",
            Status = true,
            Data = data
        };

    public async Task<ServiceResponse<PagedList<T>>> PagedResponseAsync<T>(IQueryable<T> source,
        int page, int pageSize, string entityName) where T : class
    {
        var count = await source.CountAsync();
        var items = count > 0 ? await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync() : null;
        var pagedResp = new PagedList<T>(items, count, page, pageSize, entityName);
        return new ServiceResponse<PagedList<T>>
        {
            Message = pagedResp.Summary,
            Status = true,
            Data = pagedResp
        };
    }
}