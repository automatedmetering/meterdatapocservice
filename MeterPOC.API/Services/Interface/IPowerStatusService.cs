public interface IPowerStatusService
{
    Task<ServiceResponse<string>> CreateAsync(CreatePowerStatusDTO model);
    Task<ServiceResponse<PagedList<PowerStatusDataDTO>>> GetAsync(int page, int size);
}