public interface IConsumptionService
{
    Task<ServiceResponse<string>> AddAsync(AddConsumptionDTO model);
    Task<ServiceResponse<PagedList<ConsumptionDTO>>> GetAsync(int page = 1, int size = 10);
}