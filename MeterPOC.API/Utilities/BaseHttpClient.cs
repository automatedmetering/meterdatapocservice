using System.Net.Http.Headers;
using System.Text;

public interface IBaseHttpClient
{
    Task<ServiceResponse<T>> JSendPostAsync<T>(string baseUrl, object postdata, string url, string token = null, string apikey = null);
    Task<ServiceResponse<T>> JSendGetAsync<T>(string baseUrl, string url, string token = null, string apikey = null);

    Task<T> GetAsync<T>(string baseUrl, string url, string token = null, string apikey = null);
    Task<T> PostAsync<T>(string baseUrl, object postdata, string url, string token = null, string apikey = null);

    Task PostFormEncodedAsync(string baseUrl, Dictionary<string, string> postdata, string url,
        string token = null, string apikey = null);

    Task<T> FlutterWavePostAsync<T>(string baseUrl, object postdata, string url, string secreteKey);
    Task<T> FlutterWaveGetAsync<T>(string baseUrl, string url, string secreteKey);
}

public class BaseHttpClient : IBaseHttpClient
{
    private readonly IHttpClientFactory _httpClientFactory;
    public BaseHttpClient(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public virtual async Task<ServiceResponse<T>> JSendGetAsync<T>(string baseUrl, string url, 
        string token = null, string apikey = null)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        url = url is not null ? $"{baseUrl}{url}" : baseUrl;
        using var request = new HttpRequestMessage(HttpMethod.Post, url);
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        if (httpResponse.IsSuccessStatusCode)
        {
            returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
            return new ServiceResponse<T> { Status = true, Message = "success", Data = returnValue };
        }
        return new ServiceResponse<T> { Message = httpResponse.ReasonPhrase };

    }

    public async Task<ServiceResponse<T>> JSendPostAsync<T>(string baseUrl, object postdata, string url, 
        string token = null, string apikey = null)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        url = $"{baseUrl}{url}";
        using var request = new HttpRequestMessage(HttpMethod.Post, url)
        {
            Content = new StringContent(postdata.ToJson(), Encoding.UTF8, "application/json")
        };
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
        var responseString = await httpResponse.Content.ReadAsStringAsync();
        returnValue = responseString.FromJson<T>();
        return new ServiceResponse<T> { Status = true, Message = "success", Data = returnValue };

    }

    public virtual async Task<T> GetAsync<T>(string baseUrl, string url, 
        string token = null, string apikey = null)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        url = $"{baseUrl}{url}";
        using var request = new HttpRequestMessage(HttpMethod.Post, url);
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        return returnValue;

    }

    public async Task<T> PostAsync<T>(string baseUrl, object postdata, string url, 
        string token = null, string apikey = null)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        
        if (!string.IsNullOrEmpty(apikey)) client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        url = url is not null ? $"{baseUrl}{url}" : baseUrl;
        using var request = new HttpRequestMessage(HttpMethod.Post, url)
        {
            Content = new StringContent(postdata.ToJson(), Encoding.UTF8, "application/json")
        };
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
        var resp = await httpResponse.Content.ReadAsStringAsync();
        returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        return returnValue;
    }

    public async Task<T> FlutterWavePostAsync<T>(string baseUrl, object postdata, string url, string secreteKey)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(secreteKey))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", secreteKey);

        url = $"{baseUrl}{url}";
        using var request = new HttpRequestMessage(HttpMethod.Post, url)
        {
            Content = new StringContent(postdata.ToJson(), Encoding.UTF8, "application/json")
        };
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
        returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        return returnValue;
    }

    public virtual async Task<T> FlutterWaveGetAsync<T>(string baseUrl, string url, string secreteKey)
    {
        T returnValue;

        using var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(secreteKey))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", secreteKey);

        url = $"{baseUrl}{url}";
        using var request = new HttpRequestMessage(HttpMethod.Get, url);
        using var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        return returnValue;
    }

    public virtual async Task PostFormEncodedAsync(string baseUrl, Dictionary<string, string> postdata, string url, 
        string token = null, string apikey = null)
    {
        using var client = _httpClientFactory.CreateClient();
        
        if (!string.IsNullOrEmpty(apikey)) client.DefaultRequestHeaders.Add("apiKey", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        url = url is not null ? $"{baseUrl}{url}" : baseUrl;

        client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
        using var request = new HttpRequestMessage(HttpMethod.Post, url);
        using var content = new FormUrlEncodedContent(postdata);
        request.Content = content;
        using var response = await client.SendAsync(request).ConfigureAwait(false);
    }
}
