using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

public static class Utilities
{
    private static readonly JsonSerializerOptions jsonOptions = new JsonSerializerOptions()
    {
        PropertyNameCaseInsensitive = true,
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        IgnoreReadOnlyProperties = false
    };

    public static IDictionary<string, object> ToDictionary(this object source, 
        BindingFlags bindingAttr = BindingFlags.DeclaredOnly 
        | BindingFlags.Public | BindingFlags.Instance)
    {
        return source.GetType().GetProperties(bindingAttr).ToDictionary
        (
            propInfo => propInfo.Name,
            propInfo => propInfo.GetValue(source, null)
        );
    }

    public static string Ref(int lenght = 10, bool isWithTimeStamp = false)
    {
        var reference = $"{Guid.NewGuid().ToString().Replace("-", "").Remove(lenght).ToUpper()}";
        return isWithTimeStamp ? $"{DateTime.Now:ssmmhhddMMyy}{reference}" : reference;
    }

    public static async Task<T> DeserializeRequestAsync<T>(HttpResponseMessage response)
    {
        using var contentStream = await response.Content.ReadAsStreamAsync();
        var result = await JsonSerializer.DeserializeAsync<T>(contentStream, jsonOptions);
        return result;
    }

    public static string EncodeToken(this string input) =>input.Replace("+", "PLS4PLS")
                .Replace("/", "SLS5PLS").Replace("==", "EQL9XPL");

    public static string DecodeToken(this string input) => input.Replace("PLS4PLS", "+")
                .Replace("SLS5PLS", "/").Replace("EQL9XPL","==");

    public static string EncodeEmail(this string input) => input.Replace("@", "P39nW")
        .Replace(".", "q9sd").Replace("com", "33oWe").Replace("mail", "s2233w");

    public static string DecodeEmail(this string input) => input.Replace("P39nW", "@")
        .Replace("q9sd", ".").Replace("33oWe", "com").Replace("s2233w", "mail");

    public static string ToJson(this object input) => JsonSerializer.Serialize(input, jsonOptions);

    public static T FromJson<T>(this string input) => JsonSerializer.Deserialize<T>(input, jsonOptions);


    public static bool IsValidEmail(this string email)
    {
        try
        {
            if (email.Contains("#") || email.Contains("^") || email.Contains("&")
                || email.Contains("%") || email.Contains("$") || email.Contains("*")) return false;
            if (new MailAddress(email).Address != email) return false;

            var indexOfAt = email.Split('@')[1];
            if (!indexOfAt.Contains('.')) return false;
            var indexOfDot = indexOfAt.Split('.')[1];
            return !string.IsNullOrEmpty(indexOfDot);
        }
        catch
        {
            return false;
        }
    }

    public static bool IsValidUserName(this string userName)
    {
        try
        {
            userName = userName.Trim();
            if (userName.Contains("#") || userName.Contains("^") || userName.Contains("&")
                || userName.Contains("%") || userName.Contains("$") || userName.Contains("*")
                || userName.Contains(".") || userName.Contains("@")) return false;
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static string TrimAllSpace(this string input) => Regex.Replace(input, @"\s+", "");

    public static string SHA512String(this string inputString)
    {
        SHA512 sha512 = SHA512.Create();
        byte[] bytes = Encoding.UTF8.GetBytes(inputString);
        byte[] byteArray = sha512.ComputeHash(bytes);

        var stringResult = new StringBuilder();
        for (int i = 0; i < byteArray.Length; i++)
        {
            stringResult.Append(byteArray[i].ToString("X2"));
        }
        return stringResult.ToString();
    }

    public static string Base64Encode(this string plainText)
    {
        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        return Convert.ToBase64String(plainTextBytes);
    }

    public static string Base64Decode(this string base64EncodedData)
    {
        var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return Encoding.UTF8.GetString(base64EncodedBytes);
    }

    public static string ToTitleCase(this string input)
    {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
    }

    public static List<string> DefaultURL => new List<string> { "www", "quikfud" };

    public static List<T> Shuffle<T>(this List<T> list)
    {
        var rng = new Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
        return list;
    }

    public static string Name(this Enum e)
    {
        try
        {
            var attributes = (DisplayAttribute[])e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false);
            return attributes.Length > 0 ? attributes[0].Name : string.Empty;
        }
        catch
        {
            return "NA";
        }
    }
}
