using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<PowerStatusData> PowerStatusDatas { get; set; }
    public virtual DbSet<Consumption> Consumptions { get; set; }
    

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<PowerStatusData>().HasQueryFilter(p => !p.IsDeleted);
        modelBuilder.Entity<Consumption>().HasQueryFilter(p => !p.IsDeleted);
        
        modelBuilder.Entity<PowerStatusData>().HasIndex(b => b.MeterNumber);
        modelBuilder.Entity<Consumption>().HasIndex(b => b.MeterNumber);
    }
}