using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration["ConnectionStrings:DatabaseDetails"];

builder.Services.AddDbContext<AppDbContext>(options => options.UseNpgsql(connectionString));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IResponseService, ResponseService>();
builder.Services.AddScoped<IPowerStatusService, PowerStatusService>();
builder.Services.AddScoped<IConsumptionService, ConsumptionService>();

await using var app = builder.Build();

app.UseSwagger();

app.MapPost("/PostStatus", async ([FromBody] CreatePowerStatusDTO model, IPowerStatusService _powerStatusService) =>
    await _powerStatusService.CreateAsync(model))
    .Produces<ServiceResponse<string>>(StatusCodes.Status200OK);

app.MapGet("/PowerStatus", async (IPowerStatusService _powerStatusService, int page, int size) =>
    await _powerStatusService.GetAsync(page, size))
    .Produces<ServiceResponse<PagedList<PowerStatusDataDTO>>>(StatusCodes.Status200OK);

app.MapPost("/Consumption", async ([FromBody] AddConsumptionDTO model, IConsumptionService _consumptionService) =>
    await _consumptionService.AddAsync(model))
    .Produces<ServiceResponse<string>>(StatusCodes.Status200OK);

app.MapGet("/Consumption", async (IConsumptionService _consumptionService, int page, int size) =>
    await _consumptionService.GetAsync(page, size))
    .Produces<ServiceResponse<PagedList<ConsumptionDTO>>>(StatusCodes.Status200OK);

app.UseSwaggerUI(c =>
{
    c.RoutePrefix = "";
    c.DefaultModelsExpandDepth(-1);
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
});
ApplyMigrations(app);
app.Run();

static void ApplyMigrations(WebApplication app)
{
    using var scope = app.Services.CreateScope();
    var db = scope.ServiceProvider.GetRequiredService<AppDbContext>();
    db.Database.Migrate();
}