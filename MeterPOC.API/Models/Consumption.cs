public class Consumption : BaseEntity
{
    public int MeterNumber { get; set; }
    public double ReadingKwH { get; set; }
}