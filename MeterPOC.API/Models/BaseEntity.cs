using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public abstract class BaseEntity
{
    public BaseEntity()
    {
        Created = DateTime.UtcNow;
        Modified = Created;
    }
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }
    public DateTimeOffset Created { get; set; }
    public DateTimeOffset? Modified { get; set; }
    public string CreatedBy { get; set; }
    public DateTimeOffset? Deleted { get; set; }
    public bool IsDeleted { get; set; }
    public string DeletedBy { get; set; }
}