public class PowerStatusData : BaseEntity
{
    public PowerStatus Status { get; set; }
    public string MeterNumber { get; set; }
    public bool IsLightUp { get; set; }
}